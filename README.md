# vue components

Various vue components

```
git clone https://gitlab.com/sergey256r/vue-components
cd vue-components

```

=======
# dental

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
